# Database migrations

In Visual Studio Package Manager Console need execute next commands

```
PM> enable-migrations
```

```
PM> add-migration Init
```

```
PM> update-database
```

To start program, need to change connection string in app.config and run only last command.