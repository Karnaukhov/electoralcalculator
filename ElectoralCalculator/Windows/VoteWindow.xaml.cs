﻿using ElectoralCalculator.ViewModels;
using System.Windows;

namespace ElectoralCalculator
{
    public partial class VoteWindow : Window
    {
        public VoteWindow(VoteWindowsViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
