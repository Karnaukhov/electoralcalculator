﻿using ElectoralCalculator.ViewModels;

namespace ElectoralCalculator
{
    public partial class LoginWindow
    {
        public LoginWindow(LoginWindowViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
