﻿using ElectoralCalculator.ViewModels;
using System.Windows;

namespace ElectoralCalculator
{
    public partial class VoteResultWindow : Window
    {
        public VoteResultWindow(VoteResultWindowViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
