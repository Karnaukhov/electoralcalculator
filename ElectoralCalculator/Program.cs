﻿using System;
using ElectoralCalculator.Data;
using ElectoralCalculator.Data.Repositories;
using ElectoralCalculator.ViewModels;
using NLog;
using SimpleInjector;

namespace ElectoralCalculator
{

    public class Program
    {
        [STAThread]
        static void Main()
        {
            var container = Bootstrap();

            RunApplication(container);
        }

        private static Container Bootstrap()
        {
            var container = new Container();

            container.Register<CurrentUserInfo>(Lifestyle.Singleton);

            container.Register<ILogger>(() => LogManager.GetLogger(System.Diagnostics.Process.GetCurrentProcess().ProcessName));

            container.Register(() => new HttpWrapper(System.Configuration.ConfigurationManager.AppSettings["BaseUrl"]));

            container.Register<ApplicationContext>(Lifestyle.Singleton);
            container.Register<UserRepository>();
            container.Register<CandidateRepository>();

            container.Register<LoginWindow>();
            container.Register<LoginWindowViewModel>();

            container.Register<VoteWindow>();
            container.Register<VoteWindowsViewModel>();

            container.Register<VoteResultWindow>();
            container.Register<VoteResultWindowViewModel>();

            container.Verify();

            return container;
        }

        private static void RunApplication(Container container)
        {
            try
            {
                var app = new App();
                var voteWinow = container.GetInstance<VoteWindow>();
                voteWinow.Visibility = System.Windows.Visibility.Hidden;

                var loginWindow = container.GetInstance<LoginWindow>();
                var model = (LoginWindowViewModel)loginWindow.DataContext;
                model.Logined += (sender, e) =>
                {
                    if (e.Success)
                    {
                        loginWindow.Close();
                        voteWinow.Show();

                        if(e.Voted)
                        {
                            var voteResultWinow = container.GetInstance<VoteResultWindow>();
                            voteResultWinow.Show();
                        }
                    }
                };

                loginWindow.Show();

                app.Run(voteWinow);
            }
            catch (Exception ex)
            {
                var logger = container.GetInstance<ILogger>();
                logger.Fatal(ex);
            }
        }
    }
}
