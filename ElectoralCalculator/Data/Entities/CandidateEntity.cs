﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectoralCalculator.Data.Entities
{
    [Table("Candidates")]
    public class CandidateEntity : IEntity
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Party { get; set; }

        public ICollection<UserEntity> Users { get; set; }

        public override string ToString() => $"{Name} - {Party}";
    }
}
