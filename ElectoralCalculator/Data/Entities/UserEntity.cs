﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElectoralCalculator.Data.Entities
{
    [Table("Users")]
    public class UserEntity : IEntity
    {
        public long Id { get; set; }

        [Display(Name = "Fore name")]
        public string ForeName { get; set; }

        [Display(Name = "Sur name")]
        public string SurName { get; set; } 

        [Display(Name = "PESEL")]
        public string Pesel { get; set; }

        public CandidateEntity Candidate { get; set; }
    }
}
