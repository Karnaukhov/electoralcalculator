﻿using ElectoralCalculator.Data.Entities;
using NLog;
using System;
using System.Linq;

namespace ElectoralCalculator.Data.Repositories
{
    public sealed class UserRepository : BaseRepository<UserEntity>
    {
        public UserRepository(ApplicationContext context, ILogger logger) : base(context, logger)
        {
        }

        internal UserEntity IsExists(UserEntity entity)
        {
            try
            {
                var result = DbSet.FirstOrDefault(x => x.Pesel == entity.Pesel && x.SurName == entity.SurName && x.ForeName == x.ForeName);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                throw;
            }
        }

        internal void Vote(long currentUserId, CandidateEntity entity)
        {
            try
            {
                var user = DbSet.First(x => x.Id == currentUserId);
                user.Candidate = entity;
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                throw;
            }
        }
    }
}
