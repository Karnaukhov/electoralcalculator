﻿using ElectoralCalculator.Data.Entities;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ElectoralCalculator.Data.Repositories
{
    public class BaseRepository<TEntity> where TEntity : class, IEntity
    {
        public BaseRepository(ApplicationContext context, ILogger logger)
        {
            Context = context;
            Logger = logger;
            DbSet = Context.Set<TEntity>();
        }

        protected ApplicationContext Context { get; }
        protected ILogger Logger { get; }
        protected DbSet<TEntity> DbSet { get; }

        public void Create(TEntity item)
        {
            try
            {
                DbSet.Add(item);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                throw;
            }
        }

        public IEnumerable<TEntity> Read(string include = null)
        {
            try
            {
                return DbSet.OptionalInclude(include).Select(x => x);
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                throw;
            }
        }

        public TEntity Read(long id)
        {
            try
            {
                return DbSet.FirstOrDefault(x => x.Id == id);
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                throw;
            }
        }

        public void Update(long id, TEntity item)
        {
            try
            {
                var findItem = DbSet.FirstOrDefault(x => x.Id == id);

                if (findItem is null)
                    throw new Exception("Item not found");

                item.Id = id;
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                throw;
            }
        }

        public void Delete(long id)
        {
            try
            {
                var item = DbSet.FirstOrDefault(x => x.Id == id);

                if (item is null)
                    throw new Exception("Item not found");

                DbSet.Remove(item);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                throw;
            }
        }
    }
}
