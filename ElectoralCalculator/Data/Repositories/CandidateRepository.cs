﻿using ElectoralCalculator.Data.Entities;
using NLog;

namespace ElectoralCalculator.Data.Repositories
{
    public sealed class CandidateRepository : BaseRepository<CandidateEntity>
    {
        public CandidateRepository(ApplicationContext context, ILogger logger) : base(context, logger)
        {
        }
    }
}
