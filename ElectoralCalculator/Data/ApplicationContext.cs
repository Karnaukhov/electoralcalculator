﻿using ElectoralCalculator.Data.Entities;
using System.Data.Entity;

namespace ElectoralCalculator.Data
{

    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("DefaultConnectionString")
        {
        }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<CandidateEntity> Candidates { get; set; }
    }
}
