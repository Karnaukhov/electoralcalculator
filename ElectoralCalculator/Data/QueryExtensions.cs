﻿using System.Data.Entity;
using System.Linq;

namespace ElectoralCalculator.Data
{
    internal static class QueryExtensions
    {
        internal static IQueryable<T> OptionalInclude<T>(this IQueryable<T> query, string include) where T : class
        {
            return query = include != null ? query.Include(include) : query;
        }
    }
}
