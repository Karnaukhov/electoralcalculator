﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ElectoralCalculator.Http.Entities
{
    public class Candidate
    {
        public string Name { get; set; }
        public string Party { get; set; }
    }

    [JsonObject("Candidates")]
    public class RootCandidates
    {
        public string PublicationDate { get; set; }

        [JsonProperty("candidate")]
        public List<Candidate> Candidates { get; set; }
    }

    public class RootCandidate
    {
        public RootCandidates Candidates { get; set; }
    }
}
