﻿using System.Collections.Generic;

namespace ElectoralCalculator.Http.Entities
{
    public class Person
    {
        public string Pesel { get; set; }
    }

    public class Disallowed
    {
        public string PublicationDate { get; set; }
        public List<Person> Person { get; set; }
    }

    public class RootDisallowed
    {
        public Disallowed Disallowed { get; set; }
    }
}
