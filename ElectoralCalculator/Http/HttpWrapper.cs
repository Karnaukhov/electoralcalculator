﻿using ElectoralCalculator.Http.Entities;
using Newtonsoft.Json;
using RestSharp;

namespace ElectoralCalculator
{
    public class HttpWrapper
    {
        public HttpWrapper(string baseUrl)
        {
            BaseUrl = baseUrl;
        }

        private string BaseUrl { get; }

        public RootCandidate GetCandidates() => GetHelper<RootCandidate>("candidates");

        public RootDisallowed GetBlocked() => GetHelper<RootDisallowed>("blocked");

        private T GetHelper<T>(string url)
        {
            var client = new RestClient(BaseUrl);
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("Accept", "application/json");

            var response = client.Get(request);
            var content = response.Content;

            return JsonConvert.DeserializeObject<T>(content);
        }
    }
}
