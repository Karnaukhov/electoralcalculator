﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Markup;

namespace ElectoralCalculator
{
    public class DisplayNameExtension : MarkupExtension
    {
        public Type Type { get; set; }

        public string PropertyName { get; set; }

        public DisplayNameExtension() { }
        public DisplayNameExtension(string propertyName)
        {
            PropertyName = propertyName;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            var property = Type.GetProperty(PropertyName);
            var attribute = property.GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault();
            return attribute != null
                ? (attribute as DisplayAttribute).Name
                : "Invalid property display name";
        }
    }
}
