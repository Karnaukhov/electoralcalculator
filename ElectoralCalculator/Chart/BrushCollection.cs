﻿using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows.Media;

namespace ElectoralCalculator.Chart
{
    public class BrushCollection : ObservableCollection<Brush>
    {
        public BrushCollection()
        {
            var _brush = typeof(Brushes);
            var props = _brush.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                var _color = (Brush)prop.GetValue(null, null);
                if (_color != Brushes.LightSteelBlue && _color != Brushes.White &&
                     _color != Brushes.WhiteSmoke && _color != Brushes.LightCyan &&
                     _color != Brushes.LightYellow && _color != Brushes.Linen)
                    Add(_color);
            }
        }
    }
}
