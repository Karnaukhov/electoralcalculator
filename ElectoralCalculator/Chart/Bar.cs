﻿
namespace ElectoralCalculator.Chart
{
    public class Bar
    {
        public string BarName { set; get; }
        public int Value { set; get; }
    }
}
