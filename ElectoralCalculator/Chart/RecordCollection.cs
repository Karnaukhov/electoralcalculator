﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ElectoralCalculator.Chart
{
    public class RecordCollection : ObservableCollection<Record>
    {

        public RecordCollection(List<Bar> barvalues)
        {
            var rand = new Random();
            var brushcoll = new BrushCollection();

            foreach (Bar barval in barvalues)
            {
                int num = rand.Next(brushcoll.Count / 3);
                Add(new Record(barval.Value, brushcoll[num], barval.BarName));
            }
        }

    }
}
