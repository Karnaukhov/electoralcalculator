﻿using ElectoralCalculator.Data.Entities;
using ElectoralCalculator.Data.Repositories;
using NLog;
using PESEL.Models;
using PESEL.Validators.Impl;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace ElectoralCalculator.ViewModels
{
    public class LoginWindowViewModel : CommonWindowViewModel
    {
        private UserRepository Repository { get; }
        private CurrentUserInfo CurrentUserInfo { get; }
        private HttpWrapper Wrapper { get; }
        private SimpleInjector.Container Container { get; }

        public LoginWindowViewModel(ILogger logger, UserRepository userRepository, CurrentUserInfo currentUserInfo, HttpWrapper wrapper, SimpleInjector.Container container) : base(logger)
        {
            Title = "Login";

            Repository = userRepository;
            CurrentUserInfo = currentUserInfo;
            Wrapper = wrapper;
            Container = container;
            LoginCommand = new RelayCommand<UserEntity>(Login, CanLogin);
        }

        public UserEntity CurrentUser { get; set; } = new UserEntity();
        public ICommand LoginCommand { get; set; }

        private void Login(UserEntity entity)
        {
            if (!ValidatePesel(entity.Pesel))
            {
                MessageBox.Show($"Invalid PESEL number.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var blockedList = Wrapper.GetBlocked();
            if (blockedList == null)
            {
                MessageBox.Show($"Some problem with getting blocked list", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var blocked = blockedList.Disallowed.Person.Any(x => x.Pesel.Equals(entity.Pesel));
            if (blocked)
                MessageBox.Show("Unfortunately, you can not login because you are locked.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);

            var existsUser = Repository.IsExists(entity);

            if (existsUser == null)
            {
                Repository.Create(entity);
                CurrentUserInfo.CurrentUserId = entity.Id;
            }
            else
            {
                CurrentUserInfo.CurrentUserId = existsUser.Id;
            }

            var voted = existsUser?.Candidate != null;

            if (voted)
            {
                MessageBox.Show($"You have already vote for {existsUser.Candidate}.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            OnLogin(new LoginEventArgs(true, voted, blocked));
        }

        private bool CanLogin(UserEntity entity)
        {
            return !string.IsNullOrEmpty(entity?.SurName) && !string.IsNullOrEmpty(entity?.ForeName) && !string.IsNullOrEmpty(entity?.Pesel);
        }

        private bool ValidatePesel(string pesel)
        {
            var validator = new PeselValidator();
            var ent = new Entity(pesel);
            var validationResult = validator.Validate(ent);

            if (!validationResult.IsValid || (DateTime.MinValue + (DateTime.Now - validationResult.Pesel.BirthDate)).AddYears(-1).AddMonths(-1).AddDays(-1).Year < 18)
                return false;

            return true;
        }

        public event EventHandler<LoginEventArgs> Logined;

        protected virtual void OnLogin(LoginEventArgs e)
        {
            Logined?.Invoke(this, e);
        }
    }

    public class LoginEventArgs : EventArgs
    {
        public readonly bool Success;
        public readonly bool Voted;
        public readonly bool Blocked;
        public LoginEventArgs(bool success, bool voted, bool blocked)
        {
            Success = success;
            Voted = voted;
            Blocked = blocked;
        }
    }
}