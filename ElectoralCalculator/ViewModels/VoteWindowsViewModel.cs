﻿using ElectoralCalculator.Data.Entities;
using ElectoralCalculator.Data.Repositories;
using NLog;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace ElectoralCalculator.ViewModels
{
    public sealed class VoteWindowsViewModel : CommonWindowViewModel
    {
        private CandidateEntity _selectedcandidate;

        private UserRepository UserRepository { get; }
        private CandidateRepository Repository { get; }
        private CurrentUserInfo CurrentUserInfo { get; }
        private SimpleInjector.Container Container { get; }

        public VoteWindowsViewModel(ILogger logger, UserRepository userRepository, CandidateRepository repository, CurrentUserInfo currentUserInfo, SimpleInjector.Container container) : base(logger)
        {
            Title = "Vote";

            UserRepository = userRepository;
            Repository = repository;
            CurrentUserInfo = currentUserInfo;
            Container = container;
            foreach (var item in Repository.Read())
            {
                Candidates.Add(item);
            }

            VoteCommand = new RelayCommand<CandidateEntity>(Vote, CanVote);
        }

        public ICommand VoteCommand { get; set; }

        public ObservableCollection<CandidateEntity> Candidates { get; set; } = new ObservableCollection<CandidateEntity>();

        public CandidateEntity SelectedCandidate
        {
            get => _selectedcandidate; set
            {
                _selectedcandidate = value;
                RaisePropertyChanged();
            }
        }

        private void Vote(CandidateEntity entity)
        {
            var dialog = MessageBox.Show($"Do you really want to vote for {entity}?", "Question", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (dialog != MessageBoxResult.OK)
            {
                SelectedCandidate = null;
                return;
            }

            UserRepository.Vote(CurrentUserInfo.CurrentUserId, entity);
            var voteresultVindow = Container.GetInstance<VoteResultWindow>();
            voteresultVindow.Show();
        }

        private bool CanVote(CandidateEntity entity)
        {
            var currentUser = UserRepository.Read(CurrentUserInfo.CurrentUserId);
            if (currentUser == null)
                return false;

            return currentUser.Candidate == null;
        }
    }
}