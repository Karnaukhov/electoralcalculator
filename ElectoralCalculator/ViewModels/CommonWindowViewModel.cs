﻿using NLog;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ElectoralCalculator.ViewModels
{
    public class CommonWindowViewModel : INotifyPropertyChanged
    {
        public CommonWindowViewModel(ILogger logger)
        {
            Logger = logger;
        }

        public string Title { get; set; } = "Unnamed";

        protected ILogger Logger { get; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
