﻿using ElectoralCalculator.Data.Entities;
using ElectoralCalculator.Data.Repositories;
using NLog;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using ServiceStack;
using Microsoft.Win32;
using System;
using System.Windows;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Collections.Generic;
using ElectoralCalculator.Chart;

namespace ElectoralCalculator.ViewModels
{
    public sealed class VoteResultWindowViewModel : CommonWindowViewModel
    {
        public VoteResultWindowViewModel(ILogger logger, CandidateRepository candidateRepository) : base(logger)
        {
            Title = "Vote result";
            var candidates = candidateRepository.Read(nameof(CandidateEntity.Users));

            var perCandidate = candidates.Select(x => new { x.Name, x.Party, Votes = x.Users.Count }).ToList();
            perCandidate.ForEach(x => VotePerCandidate.Add(x));

            var perParty = candidates.GroupBy(x => x.Party).Select(x => new { Party = x.Key, Votes = x.SelectMany(y => y.Users).Where(y => y.Candidate != null).Count() }).ToList();
            perParty.ForEach(x => VotePerParty.Add(x));

            ExportCommand = new RelayCommand<string>(Export, (x) => true);

            BarsPerCandidate = new RecordCollection(perCandidate.Select(x => new Bar() { BarName = $"{x.Name} \r\n {x.Party}", Value = x.Votes }).ToList());
            BarsPerParty = new RecordCollection(perParty.Select(x => new Bar() { BarName = x.Party, Value = x.Votes }).ToList());
        }

        public ObservableCollection<dynamic> VotePerCandidate { get; set; } = new ObservableCollection<dynamic>();
        public ObservableCollection<dynamic> VotePerParty { get; set; } = new ObservableCollection<dynamic>();

        public RecordCollection BarsPerCandidate { get; set; } 
        public RecordCollection BarsPerParty { get; set; } 

        public ICommand ExportCommand { get; set; }

        private void Export(string type)
        {
            if (type.Equals("PDF"))
            {
                Savepdf( "Vote.pdf");
            }
            else if (type.Equals("CSV"))
            {
                SaveCsv(VotePerCandidate, "Vote per candidate.csv");
                SaveCsv(VotePerParty, "Vote per party.csv");
            }
        }

        private void Savepdf(string fileName)
        {
            var dlg = new SaveFileDialog
            {
                FileName = fileName
            };
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    var pdfDoc = new Document();
                    string path = dlg.FileName ;
                    var writer = PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.OpenOrCreate));
                    pdfDoc.Open();

                    PdfPTable tablePerCandidate = new PdfPTable(3);

                    var cellPerCandidate = new PdfPCell(new Phrase("Vote per candidate"))
                    {
                        Colspan = 3
                    };
                    tablePerCandidate.AddCell(cellPerCandidate);

                    tablePerCandidate.AddCell("Name");
                    tablePerCandidate.AddCell("Party");
                    tablePerCandidate.AddCell("Votes");

                    foreach (var item in VotePerCandidate)
                    {
                        tablePerCandidate.AddCell(item.Name);
                        tablePerCandidate.AddCell(item.Party);
                        tablePerCandidate.AddCell(item.Votes.ToString());
                    }

                    pdfDoc.Add(tablePerCandidate);
                    pdfDoc.NewPage();

                    PdfPTable tablePerParty = new PdfPTable(2);
                    var cellPerParty = new PdfPCell(new Phrase("Vote per party"))
                    {
                        Colspan = 2
                    };
                    tablePerParty.AddCell(cellPerParty);
                    tablePerParty.AddCell("Party");
                    tablePerParty.AddCell("Votes");

                    foreach (var item in VotePerParty)
                    {
                        tablePerParty.AddCell(item.Party);
                        tablePerParty.AddCell(item.Votes.ToString());
                    }

                    pdfDoc.Add(tablePerParty);

                    pdfDoc.Close();
                }
                catch (Exception ex)
                {
                    Logger.Fatal(ex);
                    MessageBox.Show("Error on saving file");
                }
            }
        }

        private void SaveCsv(ObservableCollection<dynamic> data, string fileName)
        {
            var dlg = new SaveFileDialog
            {
                FileName = fileName
            };
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    var res = GenerateCSV(data);
                    File.WriteAllText(dlg.FileName, res);
                }
                catch (Exception ex)
                {
                    Logger.Fatal(ex);
                    MessageBox.Show("Error on saving file");
                }
            }
        }

        private string GenerateCSV(ObservableCollection<dynamic> collection)
        {
            var res = collection.ToCsv<dynamic>();
            return res;
        }
    }
}
