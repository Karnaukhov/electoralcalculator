namespace ElectoralCalculator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidates",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Party = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ForeName = c.String(),
                        SurName = c.String(),
                        Pesel = c.String(),
                        Candidate_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Candidates", t => t.Candidate_Id)
                .Index(t => t.Candidate_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "Candidate_Id", "dbo.Candidates");
            DropIndex("dbo.Users", new[] { "Candidate_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.Candidates");
        }
    }
}
