using System.Data.Entity.Migrations;
using System.Linq;

namespace ElectoralCalculator.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ElectoralCalculator.Data.ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ElectoralCalculator.Data.ApplicationContext context)
        {
            // Since the list of candidates is unchanged, we will assume that it can be obtained once via REST with HttpWrapper, and then saved into the database on migration seed method.
            if (context.Candidates.Any())
                return;

            var wrapper = new HttpWrapper(System.Configuration.ConfigurationManager.AppSettings["BaseUrl"]);
            var candidates = wrapper.GetCandidates();

            var candidatesFoDb = candidates.Candidates.Candidates.Select(x => new Data.Entities.CandidateEntity
            {
                Name = x.Name,
                Party = x.Party
            });

            context.Candidates.AddRange(candidatesFoDb);
            context.SaveChanges();
        }
    }
}
